from django.shortcuts import render

def top(request):
    """トップ画面"""
    return render(request,'hungry/top.html')